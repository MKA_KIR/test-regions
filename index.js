const fs = require('fs').promises;
const parser = require('fast-xml-parser');

const optionsXMLToJSON = require('./optionsXMLToJSON');
const optionsJSONToXML = require('./optionsJSONToXML');

const Parser = parser.j2xParser
const jsToXmlParser = new Parser(optionsJSONToXML);
fs.readFile("regions.xml")
    .then(xmlData => {
        const regions = parser.parse(xmlData.toString(), optionsXMLToJSON)
        console.log(regions.ROWDATA.ROW[0])
        const regionList = regions.ROWDATA.ROW
        const regionCodeList = regionList.map(item => item.REG1_CODE)
        const regionsUniqueCodeList = new Set(regionCodeList)
        regionsUniqueCodeList.forEach(code =>{
            const region = regionList.filter(item => item.REG1_CODE === code)
            const regionXML = jsToXmlParser.parse(region)
            fs.writeFile(`regions/reg${code}.xml`, regionXML)
        })
    })

