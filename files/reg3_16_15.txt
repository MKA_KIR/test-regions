{
  "700": {
    "name": "м. Подільськ"
  },
  "001": {
    "name": "с. Андріївка"
  },
  "002": {
    "name": "селище Борщi"
  },
  "003": {
    "name": "с. Бочманiвка"
  },
  "004": {
    "name": "с. Велика Кіндратівка"
  },
  "005": {
    "name": "с. Велике Бурилове"
  },
  "006": {
    "name": "с. Великий Фонтан"
  },
  "007": {
    "name": "с. Вестерничани"
  },
  "008": {
    "name": "с. Вишневе"
  },
  "009": {
    "name": "с. Гертопи"
  },
  "010": {
    "name": "с. Гидерим"
  },
  "011": {
    "name": "с. Глибочок"
  },
  "012": {
    "name": "с. Гонората"
  },
  "013": {
    "name": "с. Гор’ївка"
  },
  "014": {
    "name": "с. Грекове Друге"
  },
  "015": {
    "name": "с. Грекове Перше"
  },
  "016": {
    "name": "с. Дiбрівка"
  },
  "017": {
    "name": "с. Домниця"
  },
  "018": {
    "name": "с. Єфросинівка"
  },
  "019": {
    "name": "с. Затишшя"
  },
  "020": {
    "name": "с. Зелений Кут"
  },
  "021": {
    "name": "с. Качурiвка"
  },
  "022": {
    "name": "с. Кирилівка"
  },
  "023": {
    "name": "с. Климентове"
  },
  "024": {
    "name": "с. Коси"
  },
  "025": {
    "name": "с. Коси-Слобідка"
  },
  "027": {
    "name": "с. Куйбишевське"
  },
  "028": {
    "name": "с. Куяльник"
  },
  "029": {
    "name": "с. Липецьке"
  },
  "030": {
    "name": "с. Любомирка"
  },
  "031": {
    "name": "с. Мала Кiндратiвка"
  },
  "032": {
    "name": "с. Мала Олександрівка"
  },
  "033": {
    "name": "с. Мала Петрівка"
  },
  "034": {
    "name": "с. Малий Фонтан"
  },
  "035": {
    "name": "с. Мардарiвка"
  },
  "036": {
    "name": "с. Миколаївка"
  },
  "037": {
    "name": "с. Миколаївка Перша"
  },
  "038": {
    "name": "с. Мурована"
  },
  "039": {
    "name": "с. Нестоїта"
  },
  "040": {
    "name": "с. Нова Кульна"
  },
  "041": {
    "name": "с. Новий Мир"
  },
  "042": {
    "name": "с. Новоселiвка"
  },
  "043": {
    "name": "с. Оброчне"
  },
  "044": {
    "name": "с. Олексiївка"
  },
  "045": {
    "name": "с. Олександрiвка"
  },
  "046": {
    "name": "с. Павлівка"
  },
  "047": {
    "name": "с. Падрецеве"
  },
  "048": {
    "name": "с. Перешори"
  },
  "049": {
    "name": "с. Петрiвка"
  },
  "050": {
    "name": "с. Поплавка"
  },
  "051": {
    "name": "с. Розалівка"
  },
  "052": {
    "name": "с. Романівка"
  },
  "053": {
    "name": "с. Соболівка"
  },
  "054": {
    "name": "с. Станiславка"
  },
  "055": {
    "name": "с. Стара Кульна"
  },
  "056": {
    "name": "с. Степанівка"
  },
  "057": {
    "name": "с. Топик"
  },
  "058": {
    "name": "с. Федорiвка"
  },
  "059": {
    "name": "с. Чапаєвка"
  },
  "060": {
    "name": "селище Чубiвка"
  },
  "061": {
    "name": "с. Ясинове"
  }
}