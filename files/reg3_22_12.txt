{
  "700": {
    "name": "м. Змiїв"
  },
  "007": {
    "name": "с. Благодатний, радгосп"
  },
  "008": {
    "name": "с. Борова"
  },
  "009": {
    "name": "с. Бутівка"
  },
  "010": {
    "name": "с. Велика Гомiльша"
  },
  "011": {
    "name": "с. Вилівка"
  },
  "012": {
    "name": "с. Вирішальний"
  },
  "013": {
    "name": "с. Водяне"
  },
  "014": {
    "name": "с. Водяхівка"
  },
  "015": {
    "name": "с. Гайдари"
  },
  "016": {
    "name": "с. Геївка"
  },
  "017": {
    "name": "с. Генiївка"
  },
  "018": {
    "name": "с. Глибока Долина"
  },
  "019": {
    "name": "с. Гришківка"
  },
  "020": {
    "name": "с. Гужвинське"
  },
  "021": {
    "name": "с. Гусинна Поляна"
  },
  "022": {
    "name": "с. Дачне"
  },
  "023": {
    "name": "с. Джгун"
  },
  "024": {
    "name": "с. Донець"
  },
  "025": {
    "name": "с. Дудківка"
  },
  "026": {
    "name": "с. Єгорівка"
  },
  "027": {
    "name": "с. Жаданівка"
  },
  "028": {
    "name": "смт. Зiдьки"
  },
  "029": {
    "name": "с. Задонецьке"
  },
  "030": {
    "name": "с. Занки"
  },
  "031": {
    "name": "с. Западня"
  },
  "033": {
    "name": "с. Камплиця"
  },
  "034": {
    "name": "с. Кирюхи"
  },
  "035": {
    "name": "с. Кисле"
  },
  "036": {
    "name": "с. Клименівка"
  },
  "037": {
    "name": "с. Козачка"
  },
  "038": {
    "name": "с. Колісники"
  },
  "039": {
    "name": "смт. Комсомольське"
  },
  "040": {
    "name": "с. Коробів Хутір"
  },
  "041": {
    "name": "с. Коропове"
  },
  "042": {
    "name": "с. Костянтівка"
  },
  "043": {
    "name": "с. Кравцове"
  },
  "044": {
    "name": "с. Красна Поляна"
  },
  "045": {
    "name": "с. Кукулівка"
  },
  "046": {
    "name": "с. Курортне"
  },
  "047": {
    "name": "с. Лазуківка"
  },
  "048": {
    "name": "с. Лісне"
  },
  "049": {
    "name": "с. Левківка"
  },
  "050": {
    "name": "с. Лиман"
  },
  "051": {
    "name": "с. Миргороди"
  },
  "052": {
    "name": "с. Мохнач"
  },
  "053": {
    "name": "с. Нижнiй Бишкин"
  },
  "054": {
    "name": "с. Омельченки"
  },
  "055": {
    "name": "с. Орджонікідзе, б/в"
  },
  "056": {
    "name": "с. Островерхiвка"
  },
  "057": {
    "name": "с. Пасiки"
  },
  "058": {
    "name": "с. Першотравневе"
  },
  "059": {
    "name": "с. Погоріле"
  },
  "060": {
    "name": "с. Пролетарське"
  },
  "061": {
    "name": "с. Роздольне"
  },
  "062": {
    "name": "с. Сидори"
  },
  "063": {
    "name": "с. Скрипаї"
  },
  "064": {
    "name": "с. Соколове"
  },
  "065": {
    "name": "с. Суха Гомільша"
  },
  "066": {
    "name": "с. Таранiвка"
  },
  "067": {
    "name": "с. Тимченки"
  },
  "068": {
    "name": "с. Тросне"
  },
  "069": {
    "name": "с. Українське"
  },
  "070": {
    "name": "с. Федорівка"
  },
  "071": {
    "name": "с. Чемужiвка"
  },
  "072": {
    "name": "с. Черемушне"
  },
  "073": {
    "name": "с. Черкаський Бишкин"
  },
  "074": {
    "name": "с. Шелудькiвка"
  },
  "075": {
    "name": "с. Шурине"
  },
  "001": {
    "name": "с. Аксютівка"
  },
  "002": {
    "name": "с. Артюхівка"
  },
  "003": {
    "name": "с. Бiрки"
  },
  "004": {
    "name": "с. Безлюдівка"
  },
  "005": {
    "name": "с. Бірочок"
  },
  "006": {
    "name": "с. Благодатне"
  },
  "076": {
    "name": "селище Безпалівка "
  }
}