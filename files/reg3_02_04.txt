{
  "700": {
    "name": "м. Гайсин"
  },
  "001": {
    "name": "с. Адамівка"
  },
  "002": {
    "name": "с. Басаличівка"
  },
  "003": {
    "name": "с. Бережне"
  },
  "004": {
    "name": "с. Бондурi"
  },
  "005": {
    "name": "с. Борсуки"
  },
  "006": {
    "name": "с. Бубнiвка"
  },
  "007": {
    "name": "с. Бур’яни"
  },
  "009": {
    "name": "с. Гнатівка"
  },
  "010": {
    "name": "с. Гранiв"
  },
  "011": {
    "name": "с. Грузьке"
  },
  "012": {
    "name": "с. Губник"
  },
  "013": {
    "name": "с. Губник, селище"
  },
  "014": {
    "name": "с. Гунча"
  },
  "015": {
    "name": "с. Дмитренки"
  },
  "016": {
    "name": "с. Жерденiвка"
  },
  "017": {
    "name": "с. Заріччя"
  },
  "018": {
    "name": "с. Зяткiвцi"
  },
  "019": {
    "name": "с. Зятківці, вокз."
  },
  "020": {
    "name": "с. Кiблич"
  },
  "021": {
    "name": "с. Карбiвка"
  },
  "022": {
    "name": "с. Карбівське"
  },
  "023": {
    "name": "с. Кисляк"
  },
  "024": {
    "name": "с. Косанове"
  },
  "025": {
    "name": "с. Кочурів"
  },
  "026": {
    "name": "с. Краснопiлка"
  },
  "027": {
    "name": "с. Крутогорб"
  },
  "028": {
    "name": "с. Кузьминцi"
  },
  "029": {
    "name": "с. Куна"
  },
  "030": {
    "name": "с. Кунка"
  },
  "031": {
    "name": "с. Кущинцi"
  },
  "032": {
    "name": "с. Ладижинськi Хутори"
  },
  "033": {
    "name": "с. Лісна Поляна"
  },
  "034": {
    "name": "с. Мiтлинцi"
  },
  "035": {
    "name": "с. Мар’янівка"
  },
  "036": {
    "name": "с. Мелешків"
  },
  "037": {
    "name": "с. Миткiв"
  },
  "038": {
    "name": "с. Михайлiвка"
  },
  "039": {
    "name": "с. Млинки"
  },
  "040": {
    "name": "с. Нараївка"
  },
  "041": {
    "name": "с. Новоселівка"
  },
  "042": {
    "name": "с. Носiвцi"
  },
  "043": {
    "name": "с. Огіївка"
  },
  "044": {
    "name": "с. Павлівка"
  },
  "045": {
    "name": "с. Рахни"
  },
  "046": {
    "name": "с. Рахнівка"
  },
  "047": {
    "name": "с. Розівка"
  },
  "048": {
    "name": "с. Семирiчка"
  },
  "049": {
    "name": "с. Сокільці"
  },
  "050": {
    "name": "с. Степашки"
  },
  "051": {
    "name": "с. Степове"
  },
  "052": {
    "name": "с. Тарасівка"
  },
  "053": {
    "name": "с. Тимар"
  },
  "054": {
    "name": "с. Тишківка"
  },
  "055": {
    "name": "с. Тишківська Слобода"
  },
  "056": {
    "name": "с. Трубочка"
  },
  "057": {
    "name": "с. Харпачка"
  },
  "058": {
    "name": "с. Хороша"
  },
  "059": {
    "name": "с. Чечелiвка"
  },
  "060": {
    "name": "с. Шура-Бондурівська"
  },
  "061": {
    "name": "с. Шура-Мітлинецька"
  },
  "062": {
    "name": "с. Щурівці"
  },
  "063": {
    "name": "с. Ярмолинцi"
  }
}