{
  "700": {
    "name": "смт. Нова Ушиця"
  },
  "001": {
    "name": "с. Iвашкiвцi"
  },
  "002": {
    "name": "с. Антонiвка"
  },
  "003": {
    "name": "с. Балабанівка"
  },
  "004": {
    "name": "с. Березiвка"
  },
  "005": {
    "name": "с. Борсуки"
  },
  "006": {
    "name": "с. Браїлiвка"
  },
  "007": {
    "name": "с. Бучая"
  },
  "008": {
    "name": "с. Вiльховець"
  },
  "009": {
    "name": "с. Вахнiвцi"
  },
  "010": {
    "name": "с. Виселок"
  },
  "011": {
    "name": "с. Глiбiв"
  },
  "012": {
    "name": "с. Глибівка"
  },
  "013": {
    "name": "с. Глибочок"
  },
  "014": {
    "name": "с. Губарів"
  },
  "015": {
    "name": "с. Гута-Глібівська"
  },
  "016": {
    "name": "с. Джуржівка"
  },
  "017": {
    "name": "с. Жабинці"
  },
  "018": {
    "name": "с. Заборознівці"
  },
  "019": {
    "name": "с. Загоряни"
  },
  "020": {
    "name": "с. Замiхiв"
  },
  "021": {
    "name": "с. Зеленi Курилiвцi"
  },
  "022": {
    "name": "с. Іванівка"
  },
  "023": {
    "name": "с. Іванківці"
  },
  "024": {
    "name": "с. Капустяни"
  },
  "025": {
    "name": "с. Каскада"
  },
  "026": {
    "name": "с. Комунар"
  },
  "027": {
    "name": "с. Косикiвцi"
  },
  "028": {
    "name": "с. Кружківці"
  },
  "029": {
    "name": "с. Куражин"
  },
  "030": {
    "name": "с. Куча"
  },
  "031": {
    "name": "с. Любомирiвка"
  },
  "032": {
    "name": "с. Мала Стружка"
  },
  "033": {
    "name": "с. Мала Щурка"
  },
  "034": {
    "name": "с. Маціорськ"
  },
  "035": {
    "name": "с. Миржіївка"
  },
  "036": {
    "name": "с. Нова Гута"
  },
  "038": {
    "name": "с. Новий Глібів"
  },
  "039": {
    "name": "с. Отрокiв"
  },
  "040": {
    "name": "с. Песець"
  },
  "041": {
    "name": "с. Пижiвка"
  },
  "042": {
    "name": "с. Пилипи- Хребтiївськi"
  },
  "043": {
    "name": "с. Пилипкiвцi"
  },
  "044": {
    "name": "с. Рудкiвцi"
  },
  "045": {
    "name": "с. Садове"
  },
  "046": {
    "name": "с. Слобiдка"
  },
  "047": {
    "name": "с. Слобода"
  },
  "048": {
    "name": "с. Соколівка"
  },
  "049": {
    "name": "с. Ставчани"
  },
  "050": {
    "name": "с. Стара Гута"
  },
  "051": {
    "name": "с. Струга"
  },
  "052": {
    "name": "с. Тимків"
  },
  "053": {
    "name": "с. Філянівка"
  },
  "054": {
    "name": "с. Хворосна"
  },
  "055": {
    "name": "с. Хребтiїв"
  },
  "056": {
    "name": "с. Цівківці"
  },
  "057": {
    "name": "с. Шебутинцi"
  },
  "058": {
    "name": "с. Шелестяни"
  },
  "059": {
    "name": "с. Щербівці"
  }
}